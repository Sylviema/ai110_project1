package fr.eql.ai110.javafx.projet1.model;

import java.io.FileNotFoundException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

import fr.eql.ai110.javafx.projet1.annuaire.AnnuaireManager;
import fr.eql.ai110.javafx.projet1.entities.Stagiaire;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class InterfaceAnnuaire extends BorderPane {
	private Label lblIntro;

	private Label lblFirstName;
	private TextField txtFirstName;
	private Label lblLastName;
	private TextField txtLastName;
	private Label lblPostCode;
	private TextField txtPostCode;
	private Label lblYearGroupName;
	private TextField txtYearGroupName;
	private Label lblYearGroup;
	private TextField txtYearGroup;

	private Button btnBack;

	private HBox btnBox3;
	private HBox btnBox;
	private HBox btnBox2;
	private GridPane btnBox4;
	private VBox btnVBox;

	private AnnuaireManager annuaire = new AnnuaireManager();
	private TableViewStagiaire tableView = new TableViewStagiaire();

	private ObservableList<Stagiaire> observableList;
	private Image logoBack = new Image(getClass().getResourceAsStream("./image/iconBack.png"));
	private Button btnSearch;
	private Image iconSearch = new Image(getClass().getResourceAsStream("./image/iconSearch.png"));
	private Button btnExport;
	private Button btnHelp;
	private Image iconHelp = new Image(getClass().getResourceAsStream("./image/iconHelp2.png"));
	private Image iconPdf = new Image(getClass().getResourceAsStream("./image/iconPdf.png"));
	private Button btnDelete;
	private Image iconDelete = new Image(getClass().getResourceAsStream("./image/iconDelet.png"));

	public InterfaceAnnuaire() {
		observableList = FXCollections.observableArrayList(annuaire.sortByAlphabeticOrder());

		btnBox = new HBox();
		btnHelp = new Button();
		btnHelp.setStyle("-fx-background-color: transparent");
		btnHelp.setGraphic(new ImageView(iconHelp));
		btnBox.setAlignment(Pos.BASELINE_RIGHT);
		btnBox.getChildren().add(btnHelp);

		lblIntro = new Label(" Annuaire Stagiaire ");
		lblIntro.setAlignment(Pos.CENTER);
		lblIntro.setTextFill(Color.DARKBLUE);
		lblIntro.setStyle("-fx-font-weight: bold; -fx-font-size: 50px");

		lblLastName = new Label("Nom : ");
		lblLastName.setStyle("-fx-font-weight: bold");
		txtLastName = new TextField();
		lblFirstName = new Label("Prénom : ");
		lblFirstName.setStyle("-fx-font-weight: bold");
		txtFirstName = new TextField();
		lblPostCode = new Label("Département : ");
		lblPostCode.setStyle("-fx-font-weight: bold");
		txtPostCode = new TextField();
		lblYearGroupName = new Label("Nom de promotion : ");
		lblYearGroupName.setStyle("-fx-font-weight: bold");
		txtYearGroupName = new TextField();
		lblYearGroup = new Label("Année de promotion : ");
		lblYearGroup.setStyle("-fx-font-weight: bold");
		txtYearGroup = new TextField();

		btnBox4 = new GridPane();
		btnBox4.setAlignment(Pos.CENTER);
		btnBox4.setHgap(20);
		btnBox4.setVgap(10);
		btnBox4.setPadding(new Insets(20));
		btnBox4.addRow(1, lblLastName, lblFirstName, lblPostCode, lblYearGroupName, lblYearGroup);
		btnBox4.addRow(2, txtLastName, txtFirstName, txtPostCode, txtYearGroupName, txtYearGroup);

		btnBox2 = new HBox(20);
		btnBox2.setAlignment(Pos.CENTER);
		btnBox2.getChildren().add(lblIntro);

		btnBack = new Button();
		btnBack.setGraphic(new ImageView(logoBack));
		btnBack.setStyle("-fx-background-color: transparent");
		btnSearch = new Button("  Rechercher");
		btnSearch.setGraphic(new ImageView(iconSearch));
		btnSearch.setStyle("-fx-font-weight: bold; -fx-background-color: transparent");
		btnExport = new Button("Exporter la liste en PDF");
		btnExport.setGraphic(new ImageView(iconPdf));
		btnExport.setStyle("-fx-font-weight: bold; -fx-background-color: transparent");
		btnDelete = new Button("Supprimer");
		btnDelete.setGraphic(new ImageView(iconDelete));
		btnDelete.setStyle("-fx-font-weight: bold; -fx-background-color: transparent");

		btnBox3 = new HBox(20);
		btnBox3.setAlignment(Pos.CENTER);

		btnVBox = new VBox(20);
		btnVBox.setAlignment(Pos.CENTER);
		btnVBox.setSpacing(20);
		btnVBox.setPrefSize(1500, 300);
		btnVBox.setStyle("-fx-background-color:white");

		btnVBox.getChildren().addAll(btnBox, btnBox2, btnBox4, btnBox3);

		btnDelete.setOnAction(event -> {

			Stagiaire deletedStagiaire = tableView.getSelectionModel().getSelectedItem();
			deleteAStagiaire(deletedStagiaire);
			tableView.getItems().remove(deletedStagiaire);

		});

		btnSearch.setOnAction(event -> {
			ObservableList obsList = tableView.getObservableList();
			RandomAccessFile raf;
			if (!txtLastName.getText().equals("")) {
				obsList.clear();
				try {
					raf = new RandomAccessFile(annuaire.getDestinationFilePath(), "rw");
					Stagiaire rootStudent = annuaire.readStudent(1, raf);
					List<Stagiaire> result = searchStudent(txtLastName.getText(), rootStudent, raf, annuaire);

					for (Stagiaire s : result) {
						if (matchWithTextfield(txtFirstName, s.getFirstName())) {
							if (matchWithTextfield(txtPostCode, s.getPostCode())) {
								if (matchWithTextfield(txtYearGroupName, s.getGroupName())) {
									if (matchWithTextfield(txtYearGroup, s.getYearGroup())) {
										obsList.add(s);
									}
								}
							}
						}

					}

				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}

			} else {

				obsList.clear();
				AnnuaireManager annuaire = new AnnuaireManager();
				List<Stagiaire> sortedStagiaires = annuaire.sortByAlphabeticOrder();

				for (Stagiaire s : sortedStagiaires) {
					if (txtLastName.getText().equals("")) {
						if (matchWithTextfield(txtFirstName, s.getFirstName())) {
							if (matchWithTextfield(txtPostCode, s.getPostCode())) {
								if (matchWithTextfield(txtYearGroupName, s.getGroupName())) {
									if (matchWithTextfield(txtYearGroup, s.getYearGroup())) {
										obsList.add(s);

									}
								}
							}
						}
					}

				}

			}

		});

		setTop(btnVBox);
		setCenter(tableView);
		setPrefSize(1300, 800);

		btnExport.setOnAction(event -> {
			ObservableList obsList = tableView.getObservableList();
			CreateSamplePDF pdf = new CreateSamplePDF();
			if (obsList.isEmpty()) {
				pdf.createSamplePDF(observableList);
			} else {
				pdf.createSamplePDF(obsList);
			}
		});
	}

	private List<Stagiaire> searchStudent(String lastName, Stagiaire rootStudent, RandomAccessFile raf,
			AnnuaireManager annuaire) {
		List<Stagiaire> list = new ArrayList<Stagiaire>();
		findStudent(lastName, rootStudent, raf, annuaire, list);

		return list;

	}

	public void findStudent(String lastName, Stagiaire stagiaire, RandomAccessFile raf, AnnuaireManager annuaire,
			List<Stagiaire> list) {
		if (stagiaire.getLastName().toUpperCase().startsWith(lastName.toUpperCase())) {

			list.add(stagiaire);

			if (annuaire.hasALeftChild(stagiaire)) {
				findStudent(lastName, annuaire.goToLeftChild(stagiaire, raf), raf, annuaire, list);

			}
			if (annuaire.hasARightChild(stagiaire)) {
				findStudent(lastName, annuaire.goToRightChild(stagiaire, raf), raf, annuaire, list);
			}

		} else {
			if (lastName.toUpperCase().compareTo(stagiaire.getLastName().toUpperCase()) > 0) {
				if (annuaire.hasARightChild(stagiaire)) {
					findStudent(lastName, annuaire.goToRightChild(stagiaire, raf), raf, annuaire, list);
				}
			} else if (lastName.toUpperCase().compareTo(stagiaire.getLastName().toUpperCase()) < 0) {
				if (annuaire.hasALeftChild(stagiaire)) {
					findStudent(lastName, annuaire.goToLeftChild(stagiaire, raf), raf, annuaire, list);

				}
			}
		}

	}

	private boolean matchWithTextfield(TextField textfield, String entry) {
		if (((entry.toUpperCase().startsWith(textfield.getText().toUpperCase())) || textfield.getText().equals(""))) {
			return true;
		} else {
			return false;
		}
	}

	public void deleteAStagiaire(Stagiaire deletedStagiaire) {

		try {
			RandomAccessFile raf = new RandomAccessFile(annuaire.getDestinationFilePath(), "rw");

			Stagiaire rootDeletedStagiaire = annuaire.readStudent(deletedStagiaire.getIndexRoot(), raf);

			Stagiaire leftChildDeletedStagiaire = new Stagiaire();

			Stagiaire rightChildDeletedStagiaire = new Stagiaire();

			// Cas 1 : Pas d'enfants
			if (!annuaire.hasALeftChild(deletedStagiaire) && !annuaire.hasARightChild(deletedStagiaire)) {
				rightOrLeftChildOfRoot(rootDeletedStagiaire, deletedStagiaire, 0, raf);

				deletedStagiaire.setIndexRoot(0);
				annuaire.writeStagiaire(deletedStagiaire, raf, deletedStagiaire.getIndexStagiaire());

				// Cas 2 : Un enfant à gauche et pas à droite
			} else if (annuaire.hasALeftChild(deletedStagiaire) && !annuaire.hasARightChild(deletedStagiaire)) {
				leftChildDeletedStagiaire = annuaire.goToLeftChild(deletedStagiaire, raf);
				onlyHasAUniqueChild(rootDeletedStagiaire, deletedStagiaire, leftChildDeletedStagiaire,
						leftChildDeletedStagiaire.getIndexStagiaire(), raf);

				// Cas 3 : Pas d'enfant à gauche et un enfant à droite
			} else if (!annuaire.hasALeftChild(deletedStagiaire) && annuaire.hasARightChild(deletedStagiaire)) {
				rightChildDeletedStagiaire = annuaire.goToRightChild(deletedStagiaire, raf);
				onlyHasAUniqueChild(rootDeletedStagiaire, deletedStagiaire, rightChildDeletedStagiaire,
						rightChildDeletedStagiaire.getIndexStagiaire(), raf);

				// Cas 4 : 2 enfants
			} else if (annuaire.hasALeftChild(deletedStagiaire) && annuaire.hasARightChild(deletedStagiaire)) {
				leftChildDeletedStagiaire = annuaire.goToLeftChild(deletedStagiaire, raf);
				rightChildDeletedStagiaire = annuaire.goToRightChild(deletedStagiaire, raf);

				rightOrLeftChildOfRoot(rootDeletedStagiaire, deletedStagiaire,
						leftChildDeletedStagiaire.getIndexStagiaire(), raf);

				leftChildDeletedStagiaire.setIndexRoot(rootDeletedStagiaire.getIndexStagiaire());

				Stagiaire newRootChild = annuaire.getTheBiggestChild(leftChildDeletedStagiaire, raf);

				newRootChild.setRightChild(rightChildDeletedStagiaire.getIndexStagiaire());
				rightChildDeletedStagiaire.setIndexRoot(newRootChild.getIndexStagiaire());

				annuaire.writeStagiaire(leftChildDeletedStagiaire, raf, leftChildDeletedStagiaire.getIndexStagiaire());
				annuaire.writeStagiaire(newRootChild, raf, newRootChild.getIndexStagiaire());
				annuaire.writeStagiaire(rightChildDeletedStagiaire, raf,
						rightChildDeletedStagiaire.getIndexStagiaire());

				resetIndexDeletedStagiaire(deletedStagiaire, raf);

			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

	private void rightOrLeftChildOfRoot(Stagiaire rootDeletedStagiaire, Stagiaire deletedStagiaire, int index,
			RandomAccessFile raf) {
		if (rootDeletedStagiaire.getRightChild() == deletedStagiaire.getIndexStagiaire()) {
			rootDeletedStagiaire.setRightChild(index);
		} else {
			rootDeletedStagiaire.setLeftChild(index);
		}
		annuaire.writeStagiaire(rootDeletedStagiaire, raf, rootDeletedStagiaire.getIndexStagiaire());
	}

	private void onlyHasAUniqueChild(Stagiaire rootDeletedStagiaire, Stagiaire deletedStagiaire,
			Stagiaire leftOrRightStagiaire, int index, RandomAccessFile raf) {
		rightOrLeftChildOfRoot(rootDeletedStagiaire, deletedStagiaire, index, raf);

		leftOrRightStagiaire.setIndexRoot(rootDeletedStagiaire.getIndexStagiaire());
		annuaire.writeStagiaire(leftOrRightStagiaire, raf, index);

		resetIndexDeletedStagiaire(deletedStagiaire, raf);

	}

	private void resetIndexDeletedStagiaire(Stagiaire deletedStagiaire, RandomAccessFile raf) {
		deletedStagiaire.setLeftChild(0);
		deletedStagiaire.setRightChild(0);
		deletedStagiaire.setIndexRoot(0);

		annuaire.writeStagiaire(deletedStagiaire, raf, deletedStagiaire.getIndexStagiaire());
	}

	public Button getBtnBack() {
		return btnBack;
	}

	public HBox getBtnBox3() {
		return btnBox3;
	}

	public Button getBtnSearch() {
		return btnSearch;
	}

	public Button getBtnExport() {
		return btnExport;
	}

	public Button getBtnDelete() {
		return btnDelete;
	}

	public TableViewStagiaire getTableView() {
		return tableView;
	}

	public void setTableView(TableViewStagiaire tableView) {
		this.tableView = tableView;
	}

	public Button getBtnHelp() {
		return btnHelp;
	}

	public void setBtnHelp(Button btnHelp) {
		this.btnHelp = btnHelp;
	}

}
