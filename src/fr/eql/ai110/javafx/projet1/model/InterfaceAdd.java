package fr.eql.ai110.javafx.projet1.model;

import java.io.FileNotFoundException;
import java.io.RandomAccessFile;
import java.util.List;

import fr.eql.ai110.javafx.projet1.annuaire.AnnuaireManager;
import fr.eql.ai110.javafx.projet1.entities.Stagiaire;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class InterfaceAdd extends VBox{
	
	private Label lblIntro;
    private Label lblLastName;
    private TextField txtLastName;
    private Label lblFirstName;
    private TextField txtFirstName;
    private Label lblPostCode;
    private TextField txtPostCode;
    private Label lblYearGroupName;
    private TextField txtYearGroupName;
    private Label lblYearGroup;
    private TextField txtYearGroup;
    
    private HBox btnBox;
    private Button btnHelp;
    private Image iconHelp = new Image(getClass().getResourceAsStream("./image/iconHelp2.png"));
//    private Image logoBack = new Image(getClass().getResourceAsStream("./image/iconBack.png"));
    private Image iconAdd = new Image(getClass().getResourceAsStream("./image/iconAdd1.png"));
    private Button btnLogoIntern;
    private Image logoIntern = new Image(getClass().getResourceAsStream("./image/iconIntern.png"));
    private Button btnAdd;
    private Label lblSuccessfullAdd;
    private Label lblAddFailed;
    private Label lblIntro1;
    private AnnuaireManager annuaire = new AnnuaireManager();
    private GridPane lblBox;

	public InterfaceAdd() {
		
		btnBox = new HBox();
		btnHelp = new Button();
		btnHelp.setStyle("-fx-background-color: transparent");
		btnHelp.setGraphic(new ImageView(iconHelp));
		btnBox.setAlignment(Pos.BASELINE_RIGHT);
		btnBox.getChildren().add(btnHelp);
		
		lblIntro1 = new Label(" Ajouter un Stagiaire ");
        lblIntro1.setAlignment(Pos.CENTER);
        lblIntro1.setTextFill(Color.DARKBLUE);
        lblIntro1.setStyle("-fx-font-weight: bold; -fx-font-size: 50px");
		
        btnLogoIntern = new Button();
        btnLogoIntern.setAlignment(Pos.CENTER);
        btnLogoIntern.setStyle("-fx-background-color: transparent");
        btnLogoIntern.setGraphic(new ImageView(logoIntern));
        lblIntro = new Label(" Veuillez saisir les informations ci-dessous : ");
        lblIntro.setTextFill(Color.DARKBLUE);
        lblIntro.setStyle("-fx-font-weight: bold; -fx-font-size: 20px");
        lblIntro.setPadding(new Insets(20));

        lblBox = new GridPane();
        lblBox.setHgap(50);
        lblBox.setVgap(20);
        lblBox.setPadding(new Insets(20));
        lblBox.setAlignment(Pos.CENTER);
        
        lblLastName = new Label("Nom : ");
        lblLastName.setStyle("-fx-font-weight: bold");
        txtLastName = new TextField();
        lblFirstName = new Label("Prénom : ");
        lblFirstName.setStyle("-fx-font-weight: bold");
        txtFirstName = new TextField();
        lblPostCode = new Label("Département : ");
        lblPostCode.setStyle("-fx-font-weight: bold");
        txtPostCode = new TextField();
        lblYearGroupName = new Label("Nom de promotion : ");
        lblYearGroupName.setStyle("-fx-font-weight: bold");
        txtYearGroupName = new TextField();
        lblYearGroup = new Label("Année de promotion : ");
        lblYearGroup.setStyle("-fx-font-weight: bold");
        txtYearGroup = new TextField();
		
        lblBox.addRow(1, lblLastName, txtLastName);
        lblBox.addRow(2, lblFirstName, txtFirstName);
        lblBox.addRow(3, lblPostCode, txtPostCode);
        lblBox.addRow(4, lblYearGroupName, txtYearGroupName);
        lblBox.addRow(5, lblYearGroup, txtYearGroup);
        lblSuccessfullAdd = new Label();
        lblSuccessfullAdd.setStyle("-fx-font-weight: bold");
        lblAddFailed = new Label();
        lblAddFailed.setTextFill(Color.RED);
        lblAddFailed.setStyle("-fx-font-weight: bold");
        btnAdd = new Button("  Ajouter");
        btnAdd.setGraphic(new ImageView(iconAdd));
        btnAdd.setStyle("-fx-background-color: transparent; -fx-font-weight: bold");
        
        getChildren().addAll(btnBox,lblIntro1, btnLogoIntern, lblIntro, lblBox, lblSuccessfullAdd, lblAddFailed);
		setStyle("-fx-background-color:white");
		setAlignment(Pos.CENTER);
		setPadding(new Insets(20));
		setPrefSize(1300, 800);;
        
		btnHelp.setOnAction(event -> {
			OpenPdf pdf = new OpenPdf();
			pdf.openPdf("C:/Users/formation/Desktop/Workspace_Projet1/Donnees/AjoutStagiaire.pdf");
		});
		
		btnAdd.setOnAction(event -> {
			 String lastName = txtLastName.getText();
	         String firstName = txtFirstName.getText();
	         String postCode = txtPostCode.getText();
	         String yearGroupName = txtYearGroupName.getText();
	         String yearGroup = txtYearGroup.getText();
	         boolean a = true;
	         boolean b = true;
	         boolean d = true;
	         
	         char[] charLastName = lastName.toCharArray();
	         for (char c : charLastName) {
	             if(!Character.isLetter(c)) {
	                 a = false;
	             }
	         }
	         char[] charFirstName = firstName.toCharArray();
	         for (char c : charFirstName) {
	             if(!Character.isLetter(c)) {
	                 b = false;
	             }
	         }
	         
	         try {
	        	 Float.parseFloat(yearGroup);
	        	 if(yearGroup.length() != 4) {
	        		 d=false;
	        	 }
	         }catch(NumberFormatException e) {
	        	 d = false;
	         }
	         
	         Stagiaire newStagiaire = new Stagiaire(lastName, firstName, postCode, yearGroupName, yearGroup,0,0,0,0);
	         List<Stagiaire> listeStagiaires = annuaire.sortByAlphabeticOrder(); 
	         int indexLastStagiaire = 1;

	         if ((lastName.equals("")) || (firstName.equals("")) || (postCode.equals("")) || (yearGroupName.equals("")) || (yearGroup.equals(""))) {
	             lblAddFailed.setText("Veuillez remplir toutes les informations afin d'ajouter un stagiaire");
	         }else if(a == false) {
	        	 lblAddFailed.setText("Veuillez entrer un nom correct");
	         }else if(b == false) {
	        	 lblAddFailed.setText("Veuillez entrer un prénom correct");
	         }else if(d == false) {
	         		lblAddFailed.setText("Veuillez entrer une année correcte 'yyyy'");
	           	 }else {
	           		 for(Stagiaire s : listeStagiaires) {
	           			 indexLastStagiaire++;
	           	}
	         int indexNewStagiaire = indexLastStagiaire;
	         try {
	             RandomAccessFile raf = new RandomAccessFile(annuaire.getDestinationFilePath(), "rw");
	             Stagiaire rootStudent = annuaire.readStudent(1, raf);
	             annuaire.compareStudent(newStagiaire, rootStudent, raf, indexNewStagiaire);

	         } catch (FileNotFoundException e) {
	             e.printStackTrace();
	         }
	         lblAddFailed.setText("");
	         lblSuccessfullAdd.setText("Le stagiaire a bien été ajouté.");
	         txtLastName.setText("");
	         txtFirstName.setText("");
	         txtPostCode.setText("");
	         txtYearGroupName.setText("");
	         txtYearGroup.setText(""); 
	         }
		});
        
	}

	public GridPane getLblBox() {
		return lblBox;
	}

	public void setLblBox(GridPane lblBox) {
		this.lblBox = lblBox;
	}

	public Button getBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(Button btnAdd) {
		this.btnAdd = btnAdd;
	}
	
	
	
	
}
