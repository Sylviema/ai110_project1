package fr.eql.ai110.javafx.projet1.model;

import java.io.FileNotFoundException;
import java.io.RandomAccessFile;

import fr.eql.ai110.javafx.projet1.annuaire.AnnuaireManager;
import fr.eql.ai110.javafx.projet1.entities.Stagiaire;

public class Recherche {

	private AnnuaireManager annuaire = new AnnuaireManager();
	private String string = "LEPANTE";
	private Stagiaire rootStagiaire = new Stagiaire();
	
	public Recherche() {
	}
	
	public void search() {

		
		try {
			RandomAccessFile raf = new RandomAccessFile(annuaire.getDestinationFilePath(), "rw");
			System.out.println("hi");
			rootStagiaire= annuaire.readStudent(1, raf);
			System.out.println("oh");
			searchStudent(raf);
			System.out.println("eh");
			
			System.out.println(searchStudent(raf));
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	private Stagiaire searchStudent(RandomAccessFile raf ) {
		if(string.compareTo(rootStagiaire.getLastName()) > 0 ) {
			rootStagiaire = annuaire.goToRightChild(rootStagiaire, raf);
			searchStudent(raf);
		}else if(string.compareTo(rootStagiaire.getLastName()) < 0 ){
			rootStagiaire = annuaire.goToLeftChild(rootStagiaire, raf);
			searchStudent(raf);
		}
		return rootStagiaire;
	}
	
}
