package fr.eql.ai110.javafx.projet1.model.admin;

import fr.eql.ai110.javafx.projet1.model.InterfaceAdd;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class InterfaceAddAdmin extends VBox{
	
	private Button btnBack;
	private Image logoBack = new Image(getClass().getResourceAsStream("../image/iconBack.png"));
	
	public InterfaceAddAdmin() {
		
		InterfaceAdd interfaceAdd = new InterfaceAdd();
        btnBack = new Button();
        btnBack.setGraphic(new ImageView(logoBack));
        btnBack.setStyle("-fx-background-color: transparent");
        interfaceAdd.getLblBox().addRow(6, btnBack,interfaceAdd.getBtnAdd());
        getChildren().add(interfaceAdd);
        
        btnBack.setOnAction(event -> {
        	InterfaceAdmin interfaceAdmin = new InterfaceAdmin();
			Scene scene = new Scene(interfaceAdmin);
			scene.getStylesheets().add(getClass().getResource("../Stylesheet.css").toExternalForm());
			Stage stage = (Stage) this.getScene().getWindow();
			stage.setScene(scene);
        });
        
        
}
}
