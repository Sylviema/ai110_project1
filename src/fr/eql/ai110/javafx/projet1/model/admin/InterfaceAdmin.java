package fr.eql.ai110.javafx.projet1.model.admin;

import fr.eql.ai110.javafx.projet1.model.OpenPdf;
import fr.eql.ai110.javafx.projet1.model.user.InterfaceUser;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class InterfaceAdmin extends VBox {

	private Button btnVisu;
	private Button btnAdd;
	private Label lblIntro;
	private GridPane btnAdGp;
	private HBox btnBox;

	private Button btnBack;
	private Button btnLogoEql;
	private HBox btnBoxAdmin;
	private Button btnAdmin;

	private Button btnHelp;
	private Image iconHelp = new Image(getClass().getResourceAsStream("../image/iconHelp2.png"));

	private Image logoEql = new Image(getClass().getResourceAsStream("../image/LogoEql.png"));
	private Image iconList = new Image(getClass().getResourceAsStream("../image/iconList.png"));
	private Image logoBack = new Image(getClass().getResourceAsStream("../image/iconBack2.png"));
	private Image iconAdd = new Image(getClass().getResourceAsStream("../image/iconAdd1.png"));
	private Image iconAdmin = new Image(getClass().getResourceAsStream("../image/iconAdmin2.png"));

	public InterfaceAdmin() {

		
		btnBox = new HBox();
		btnHelp = new Button();
		btnHelp.setStyle("-fx-background-color: transparent");
		btnHelp.setGraphic(new ImageView(iconHelp));
		btnBox.setAlignment(Pos.BASELINE_RIGHT);
		btnBox.getChildren().add(btnHelp);

		lblIntro = new Label(" Bienvenue dans votre espace administrateur ");
		lblIntro.setTextFill(Color.DARKBLUE);
		lblIntro.setStyle("-fx-font-weight: bold; -fx-font-size: 50px");
		lblIntro.setAlignment(Pos.CENTER);
		btnLogoEql = new Button();
		btnLogoEql.setStyle("-fx-background-color: transparent");
		btnLogoEql.setGraphic(new ImageView(logoEql));
		btnAdmin = new Button();
		btnAdmin.setStyle("-fx-background-color: transparent");
		btnAdmin.setGraphic(new ImageView(iconAdmin));

		btnAdGp = new GridPane();
	    btnVisu = new Button("   Annuaire stagiaire");
	    btnVisu.setPrefSize(300, 100);
	    btnVisu.setStyle("-fx-font-weight: bold");
	    btnVisu.setGraphic(new ImageView(iconList));
	    btnAdd = new Button("Ajouter un stagiaire");
	    btnAdd.setStyle("-fx-font-weight: bold");
	    btnAdd.setPrefSize(300, 100);
	    btnAdd.setGraphic(new ImageView(iconAdd));
	    btnAdGp.setHgap(50);
	    btnAdGp.setVgap(20);
	    btnAdGp.setAlignment(Pos.CENTER);
	    btnAdGp.setPadding(new Insets(20));
	    btnAdGp.addRow(1, btnVisu, btnAdd);

	    btnBack = new Button("");
	    btnBack.setGraphic(new ImageView(logoBack));
	    btnBack.setStyle("-fx-background-color: transparent");
	    btnBoxAdmin = new HBox(20);
	    btnBoxAdmin.setAlignment(Pos.CENTER);
	    btnBoxAdmin.getChildren().add(btnBack);
	    btnBoxAdmin.setPadding(new Insets(20));

	    getChildren().addAll(btnBox, btnAdmin, lblIntro, btnLogoEql, btnAdGp, btnBoxAdmin);
	    setAlignment(Pos.CENTER);
	    setPadding(new Insets(20));
	    setSpacing(20);
	    setPrefSize(1300, 800);
	    setStyle("-fx-background-color:white");
	    
	    btnHelp.setOnAction(event -> {
			OpenPdf pdf = new OpenPdf();
			pdf.openPdf("C:/Users/formation/Desktop/Workspace_Projet1/Donnees/InterfaceAdmin.pdf");
		});


		btnAdd.setOnAction(event -> {

			InterfaceAddAdmin addAdmin = new InterfaceAddAdmin();
			Scene scene = new Scene(addAdmin);
			scene.getStylesheets().add(getClass().getResource("../Stylesheet.css").toExternalForm());
			Stage stage = (Stage) this.getScene().getWindow();
			stage.setScene(scene);

		});

		btnBack.setOnAction(event -> {

			InterfaceUser interfaceUser = new InterfaceUser();
			Scene scene = new Scene(interfaceUser);
			scene.getStylesheets().add(getClass().getResource("../Stylesheet.css").toExternalForm());
			Stage stage = (Stage) this.getScene().getWindow();
			stage.setScene(scene);

		});

		btnVisu.setOnAction(event -> {

			InterfaceAnnuaireAdmin annuaireAdmin = new InterfaceAnnuaireAdmin();
			Scene scene = new Scene(annuaireAdmin);
			scene.getStylesheets().add(getClass().getResource("../Stylesheet.css").toExternalForm());
			Stage stage = (Stage) this.getScene().getWindow();
			stage.setScene(scene);

		});

	}
}
