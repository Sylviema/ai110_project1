package fr.eql.ai110.javafx.projet1.model.admin;

import fr.eql.ai110.javafx.projet1.model.InterfaceAnnuaire;
import fr.eql.ai110.javafx.projet1.model.OpenPdf;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class InterfaceAnnuaireAdmin extends BorderPane {

	public InterfaceAnnuaireAdmin() {
		
		InterfaceAnnuaire interfaceAnnuaire = new InterfaceAnnuaire();
		interfaceAnnuaire.getTableView().setEditable(true);
		
		interfaceAnnuaire.getBtnBox3().getChildren().addAll(interfaceAnnuaire.getBtnSearch(),interfaceAnnuaire.getBtnExport(), interfaceAnnuaire.getBtnDelete(), interfaceAnnuaire.getBtnBack());
		
		setCenter(interfaceAnnuaire);
		
		interfaceAnnuaire.getBtnHelp().setOnAction(event -> {
			OpenPdf pdf = new OpenPdf();
			pdf.openPdf("C:/Users/formation/Desktop/Workspace_Projet1/Donnees/AnnuaireAdmin.pdf");
		});

		interfaceAnnuaire.getBtnBack().setOnAction(event -> {
			InterfaceAdmin interfaceAdmin = new InterfaceAdmin();
			Scene scene = new Scene(interfaceAdmin);
			scene.getStylesheets().add(getClass().getResource("../Stylesheet.css").toExternalForm());
			Stage stage = (Stage) this.getScene().getWindow();
			stage.setScene(scene);
		});
	}

	
	

}
