package fr.eql.ai110.javafx.projet1.model;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

import fr.eql.ai110.javafx.projet1.annuaire.AnnuaireManager;
import fr.eql.ai110.javafx.projet1.entities.Stagiaire;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;

public class TableViewStagiaire extends TableView<Stagiaire> {

	private AnnuaireManager annuaire = new AnnuaireManager();
	private Stagiaire stagiaire;
	private ObservableList<Stagiaire> observableList = FXCollections.observableArrayList();
	private List<Stagiaire> listStagiaires = new ArrayList<Stagiaire>();

	private TableColumn<Stagiaire, String> colLastName = new TableColumn<>("Nom");
	private TableColumn<Stagiaire, String> colFirstName = new TableColumn<>("Prénom");
	private TableColumn<Stagiaire, String> colPostCode = new TableColumn<>("Département");
	private TableColumn<Stagiaire, String> colYearGroupName = new TableColumn<>("Nom de Promotion");
	private TableColumn<Stagiaire, String> colYearGroup = new TableColumn<>("Année de Promotion");
	private ToUpperCamelCase upperCamelCase = new ToUpperCamelCase();
	boolean isTrue;

	public TableViewStagiaire() {
		observableList = FXCollections.observableArrayList(annuaire.sortByAlphabeticOrder());

		colLastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
		colFirstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
		colPostCode.setCellValueFactory(new PropertyValueFactory<>("postCode"));
		colYearGroupName.setCellValueFactory(new PropertyValueFactory<>("groupName"));
		colYearGroup.setCellValueFactory(new PropertyValueFactory<>("yearGroup"));

		getColumns().addAll(colLastName, colFirstName, colPostCode, colYearGroupName, colYearGroup);
		setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

		setPrefSize(1500, 750);

		setEditable(isTrue);
		
		colLastName.setCellFactory(TextFieldTableCell.<Stagiaire>forTableColumn());
		colLastName.setOnEditCommit((CellEditEvent<Stagiaire, String> t) -> {
			System.out.println("hello");

			TablePosition<Stagiaire, String> pos = t.getTablePosition();
			String string = t.getNewValue().toUpperCase();
			int row = pos.getRow();
			stagiaire = t.getTableView().getItems().get(row);

			Stagiaire newStagiaire = new Stagiaire(string, stagiaire.getFirstName(), stagiaire.getPostCode(),
					stagiaire.getGroupName(), stagiaire.getYearGroup(), 0, 0, 0, 0);

			RandomAccessFile raf;
			try {
				raf = new RandomAccessFile(annuaire.getDestinationFilePath(), "rw");
				InterfaceAnnuaire interfaceAnnuaire = new InterfaceAnnuaire();
				AnnuaireManager annuaire = new AnnuaireManager();
				listStagiaires = annuaire.sortByAlphabeticOrder();

				interfaceAnnuaire.deleteAStagiaire(stagiaire);
				t.getTableView().getItems().remove(stagiaire);

				Stagiaire rootStudent = annuaire.readStudent(1, raf);
				annuaire.compareStudent(newStagiaire, rootStudent, raf, stagiaire.getIndexStagiaire());
				

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}

			setItems(observableList);

		});

		colFirstName.setCellFactory(TextFieldTableCell.<Stagiaire>forTableColumn());
		colFirstName.setOnEditCommit((CellEditEvent<Stagiaire, String> t) -> {
			TablePosition<Stagiaire, String> pos = t.getTablePosition();
			String string = upperCamelCase.toCamelCase((t.getNewValue()));
			int row = pos.getRow();
			stagiaire = t.getTableView().getItems().get(row);
			stagiaire.setFirstName(string);
			modifyField(stagiaire);
		});

		colPostCode.setCellFactory(TextFieldTableCell.<Stagiaire>forTableColumn());
		colPostCode.setOnEditCommit((CellEditEvent<Stagiaire, String> t) -> {
			TablePosition<Stagiaire, String> pos = t.getTablePosition();
			String string = t.getNewValue();
			int row = pos.getRow();
			stagiaire = t.getTableView().getItems().get(row);
			stagiaire.setPostCode(string);
			modifyField(stagiaire);
		});

		colYearGroupName.setCellFactory(TextFieldTableCell.<Stagiaire>forTableColumn());
		colYearGroupName.setOnEditCommit((CellEditEvent<Stagiaire, String> t) -> {
			TablePosition<Stagiaire, String> pos = t.getTablePosition();
			String string = t.getNewValue().toUpperCase();
			int row = pos.getRow();
			stagiaire = t.getTableView().getItems().get(row);
			stagiaire.setGroupName(string);
			modifyField(stagiaire);
		});

		colYearGroup.setCellFactory(TextFieldTableCell.<Stagiaire>forTableColumn());
		colYearGroup.setOnEditCommit((CellEditEvent<Stagiaire, String> t) -> {
			TablePosition<Stagiaire, String> pos = t.getTablePosition();
			String string = t.getNewValue();
			int row = pos.getRow();
			stagiaire = t.getTableView().getItems().get(row);
			stagiaire.setYearGroup(string);
			modifyField(stagiaire);

		});

		setItems(observableList);

	}

	public void modifyField(Stagiaire stagiaire) {
		try {
			RandomAccessFile raf = new RandomAccessFile(annuaire.getDestinationFilePath(), "rw");
			annuaire.writeStagiaire(stagiaire, raf, stagiaire.getIndexStagiaire());
			observableList.set(stagiaire.getIndexStagiaire(), stagiaire);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public boolean isTrue() {
		return isTrue;
	}

	public void setTrue(boolean isTrue) {
		this.isTrue = isTrue;
	}

	public ObservableList<Stagiaire> getObservableList() {
		return observableList;
	}

	public void setObservableList(ObservableList<Stagiaire> observableList) {
		this.observableList = observableList;
	}
	
	

}
