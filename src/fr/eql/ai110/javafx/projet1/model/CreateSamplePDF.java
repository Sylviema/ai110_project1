package fr.eql.ai110.javafx.projet1.model;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.property.TextAlignment;

import fr.eql.ai110.javafx.projet1.annuaire.AnnuaireManager;
import fr.eql.ai110.javafx.projet1.entities.Stagiaire;
import javafx.geometry.Pos;
import javafx.scene.text.Font;


public class CreateSamplePDF {
	
	private static String FilePath = "C:/Users/formation/Desktop/Workspace_Projet1/Donnees/Liste_Des_Stagiaires.pdf";
	
	
	public void createSamplePDF(List<Stagiaire> list) {
		
		try {
			PdfWriter writer= new PdfWriter(FilePath);
			PdfDocument pdfDoc = new PdfDocument(writer);
			Document doc = new Document(pdfDoc);
			
			doc.setTextAlignment(TextAlignment.CENTER);
			doc.setFontSize(10);
			doc.add(new Paragraph("Voici la liste des Stagiaires selon les critères séléctionnés : "));
			
			doc.setTextAlignment(TextAlignment.LEFT);
			for (Stagiaire s : list) {
				doc.add(new Paragraph(s.toString()));
			}
			
			
			if ((new File(FilePath)).exists()) {

				Process p = Runtime
				   .getRuntime()
				   .exec("rundll32 url.dll,FileProtocolHandler "+FilePath);
				p.waitFor();
					
			} else {
				System.out.println("File is not exists");

			}
			
			doc.close();
			writer.close();
			
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
		
	}


	
	
}
