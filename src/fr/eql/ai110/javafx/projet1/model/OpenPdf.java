package fr.eql.ai110.javafx.projet1.model;

import java.io.File;
import java.io.IOException;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.property.TextAlignment;

import fr.eql.ai110.javafx.projet1.entities.Stagiaire;

public class OpenPdf {

	public void openPdf(String string) {
		try {
			PdfReader reader = new PdfReader(string);
			PdfDocument pdfDoc = new PdfDocument(reader);
			Document doc = new Document(pdfDoc);

			if ((new File(string)).exists()) {

				Process p = Runtime.getRuntime().exec(
						"rundll32 url.dll,FileProtocolHandler " + string);
				p.waitFor();

			} else {
				System.out.println("File is not exists");

			}

			doc.close();
			reader.close();

		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}
}
