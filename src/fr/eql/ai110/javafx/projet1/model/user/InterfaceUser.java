package fr.eql.ai110.javafx.projet1.model.user;

import fr.eql.ai110.javafx.projet1.model.OpenPdf;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class InterfaceUser extends VBox {

	private Label lblIntro;
	private Button btnAdd;
	private Button btnHelp;
	private Image iconHelp = new Image(getClass().getResourceAsStream("../image/iconHelp2.png"));
	private Image iconAdd = new Image(getClass().getResourceAsStream("../image/iconAdd1.png"));

	private Button btnDisplay;
	private Button btnAdmin;
	private HBox btnBox2;
	private HBox btnBox1;
	private VBox lblBox;
	private GridPane btnAdGp;

	private Button btnLogoEql;
	private Image searchIcon = new Image(getClass().getResourceAsStream("../image/search_icon.png"));
	private Image logoEql = new Image(getClass().getResourceAsStream("../image/LogoEql.png"));
	private Image iconAdmin = new Image(getClass().getResourceAsStream("../image/iconAdmin.png"));
	private Image iconList = new Image(getClass().getResourceAsStream("../image/iconList.png"));

	public InterfaceUser() {

		btnBox2 = new HBox();
		btnHelp = new Button();
		btnHelp.setStyle("-fx-background-color: transparent");
		btnHelp.setGraphic(new ImageView(iconHelp));
		btnBox2.setAlignment(Pos.BASELINE_RIGHT);
		btnBox2.getChildren().add(btnHelp); 
		
		lblIntro = new Label(" Bienvenue dans l'application Annuaire ");
		lblIntro.setTextFill(Color.DARKBLUE);
		lblIntro.setStyle("-fx-font-weight: bold; -fx-font-size: 50px");
		lblIntro.setAlignment(Pos.CENTER);
		btnLogoEql = new Button();
		btnLogoEql.setStyle("-fx-background-color: transparent");
		btnLogoEql.setGraphic(new ImageView(logoEql));

		lblBox = new VBox(20);
		lblBox.setAlignment(Pos.CENTER);
		lblBox.getChildren().addAll(lblIntro, btnLogoEql);

		btnAdGp = new GridPane();
		btnDisplay = new Button(" Annuaire stagiaire");
        btnDisplay.setStyle("-fx-font-weight: bold");
        btnDisplay.setGraphic(new ImageView(iconList));
        btnDisplay.setPrefSize(300, 100);
		
		btnAdd = new Button(" Ajouter un stagiaire");
        btnAdd.setStyle("-fx-font-weight: bold");
        btnAdd.setPrefSize(300, 100);
        btnAdd.setGraphic(new ImageView(iconAdd));


		btnAdGp.addRow(1, btnDisplay, btnAdd);
		btnAdGp.setPadding(new Insets(20));
		btnAdGp.setAlignment(Pos.CENTER);
		btnAdGp.setVgap(20);
		btnAdGp.setHgap(100);

		btnBox1 = new HBox();
		btnAdmin = new Button("Connexion Administrateur");
		
		btnAdmin.setGraphic(new ImageView(iconAdmin));
		btnAdmin.setStyle("-fx-font-weight: bold");
		btnAdmin.setPrefSize(300, 100);
		btnBox1.setAlignment(Pos.CENTER);
		btnBox1.getChildren().add(btnAdmin); 
		
		getChildren().addAll(btnBox2, lblBox, btnAdGp, btnBox1);
		setPrefSize(1300, 800);
		setPadding(new Insets(50));
		setSpacing(30);

		setStyle("-fx-background-color:white");

		btnHelp.setOnAction(event -> {
			OpenPdf pdf = new OpenPdf();
			pdf.openPdf("C:/Users/formation/Desktop/Workspace_Projet1/Donnees/InterfaceUser.pdf");
		});
		
		btnAdd.setOnAction(event -> {

			InterfaceAddUser interfaceUser = new InterfaceAddUser();
			Scene scene = new Scene(interfaceUser);
			scene.getStylesheets().add(getClass().getResource("../Stylesheet.css").toExternalForm());
			Stage stage = (Stage) this.getScene().getWindow();
			stage.setScene(scene);

		});

		// bouton pour se connecter en tant qu'administrateur
		btnAdmin.setOnAction(event -> {

			ConnectionAdmin connectionAdmin = new ConnectionAdmin();
			Scene scene = new Scene(connectionAdmin);
			scene.getStylesheets().add(getClass().getResource("../Stylesheet.css").toExternalForm());
			Stage stage = (Stage) this.getScene().getWindow();
			stage.setScene(scene);

		});

		btnDisplay.setOnAction(event -> {

			InterfaceAnnuaireUser interfaceAnnuaireUser = new InterfaceAnnuaireUser();
			Scene scene = new Scene(interfaceAnnuaireUser);
			scene.getStylesheets().add(getClass().getResource("../Stylesheet.css").toExternalForm());
			Stage stage = (Stage) this.getScene().getWindow();
			stage.setScene(scene);

		});

	}


}