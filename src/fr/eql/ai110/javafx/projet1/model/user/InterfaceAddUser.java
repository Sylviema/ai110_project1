package fr.eql.ai110.javafx.projet1.model.user;

import fr.eql.ai110.javafx.projet1.model.InterfaceAdd;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class InterfaceAddUser extends VBox{

	private Button btnBack;
	private Image logoBack = new Image(getClass().getResourceAsStream("../image/iconBack.png"));
	
	public InterfaceAddUser() {
		
		InterfaceAdd interfaceAdd = new InterfaceAdd();
        btnBack = new Button();
        btnBack.setGraphic(new ImageView(logoBack));
        btnBack.setStyle("-fx-background-color: transparent");
        interfaceAdd.getLblBox().addRow(6, btnBack,interfaceAdd.getBtnAdd());
        getChildren().add(interfaceAdd);
        
        btnBack.setOnAction(event -> {
        	InterfaceUser interfaceUser = new InterfaceUser();
			Scene scene = new Scene(interfaceUser);
			scene.getStylesheets().add(getClass().getResource("../Stylesheet.css").toExternalForm());
			Stage stage = (Stage) this.getScene().getWindow();
			stage.setScene(scene);
        });
}
}
