package fr.eql.ai110.javafx.projet1.model.user;

import fr.eql.ai110.javafx.projet1.model.InterfaceAnnuaire;
import fr.eql.ai110.javafx.projet1.model.OpenPdf;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class InterfaceAnnuaireUser extends BorderPane{

	public InterfaceAnnuaireUser() {
		
		InterfaceAnnuaire interfaceAnnuaire = new InterfaceAnnuaire();
		setCenter(interfaceAnnuaire);
		interfaceAnnuaire.getTableView().setEditable(false);
		
		interfaceAnnuaire.getBtnBox3().getChildren().addAll(interfaceAnnuaire.getBtnSearch(), interfaceAnnuaire.getBtnExport(), interfaceAnnuaire.getBtnBack());
		interfaceAnnuaire.getBtnHelp().setOnAction(event -> {
			OpenPdf pdf = new OpenPdf();
			pdf.openPdf("C:/Users/formation/Desktop/Workspace_Projet1/Donnees/AnnuaireUser.pdf");
		});

		
		interfaceAnnuaire.getBtnBack().setOnAction(event -> {
			InterfaceUser interfaceUser = new InterfaceUser();
			Scene scene = new Scene(interfaceUser);
			scene.getStylesheets().add(getClass().getResource("../Stylesheet.css").toExternalForm());
			Stage stage = (Stage) this.getScene().getWindow();
			stage.setScene(scene);
		});
	}
}
