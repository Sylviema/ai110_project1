
package fr.eql.ai110.javafx.projet1.model.user;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import fr.eql.ai110.javafx.projet1.model.OpenPdf;
import fr.eql.ai110.javafx.projet1.model.admin.InterfaceAdmin;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class ConnectionAdmin extends VBox {

	private Label lblIntro;
	private Label lblIntro1;
	private Label lblUsername;
	private Label lblPassword;
	private Label lblConnectionFailed;
	private TextField txtUsername;
	private TextField txtPassword;
	private GridPane txtGp;
	private HBox btnBox;
	private Button btnHelp;
	private Image iconHelp = new Image(getClass().getResourceAsStream("../image/iconHelp2.png"));
	private Button btnAdmin;
	private Button btnIconAdmin;
	private Button btnBack;
	private Image logoBack = new Image(getClass().getResourceAsStream("../image/iconBack.png"));
    private Image iconAdmin = new Image(getClass().getResourceAsStream("../image/iconAdmin.png"));
    private Image iconConnexion = new Image(getClass().getResourceAsStream("../image/iconLock.png"));
	private String loginAdminPath = "C:/Users/formation/Desktop/Workspace_Projet1/Donnees/LoginAdmin.txt";
	

	public ConnectionAdmin() {


		btnBox = new HBox();
		btnHelp = new Button();
		btnHelp.setStyle("-fx-background-color: transparent");
		btnHelp.setGraphic(new ImageView(iconHelp));
		btnBox.setAlignment(Pos.BASELINE_RIGHT);
		btnBox.getChildren().add(btnHelp);

		lblIntro1 = new Label(" Connexion Administrateur ");
        lblIntro1.setAlignment(Pos.CENTER);
        lblIntro1.setTextFill(Color.DARKBLUE);
        lblIntro1.setStyle("-fx-font-weight: bold; -fx-font-size: 50px");
        btnIconAdmin = new Button();
        btnIconAdmin.setGraphic(new ImageView(iconAdmin));
        btnIconAdmin.setStyle("-fx-background-color: transparent");
        lblIntro = new Label(" Veuillez saisir les informations ci-dessous pour accéder à l'interface Administrateur ");
        lblIntro.setAlignment(Pos.CENTER);
        lblIntro.setTextFill(Color.DARKBLUE);
        lblIntro.setStyle("-fx-font-weight: bold; -fx-font-size: 20px");

        txtGp = new GridPane();
        lblUsername = new Label("Identifiant : ");
        lblUsername.setStyle("-fx-font-weight: bold");
        txtUsername = new TextField();
        lblPassword = new Label("Mot de passe : ");
        txtPassword = new TextField();
        lblPassword.setStyle("-fx-font-weight: bold");
		
        txtGp.setAlignment(Pos.CENTER);
        txtGp.setHgap(20);
        txtGp.setVgap(20);
        txtGp.setPadding(new Insets(20));
        txtGp.addRow(1, lblUsername, txtUsername);
        txtGp.addRow(2, lblPassword, txtPassword);
        btnAdmin = new Button("Connexion");
        btnAdmin.setStyle("-fx-font-weight: bold");
        btnAdmin.setGraphic(new ImageView(iconConnexion));
        btnAdmin.setStyle("-fx-font-weight: bold; -fx-background-color: transparent");
        btnBack = new Button();
        btnBack.setGraphic(new ImageView(logoBack));
        btnBack.setStyle("-fx-background-color: transparent");
        txtGp.addRow(3, btnBack, btnAdmin);


        lblConnectionFailed = new Label();
        lblConnectionFailed.setPadding(new Insets(20));
        lblConnectionFailed.setTextFill(Color.RED);
        lblConnectionFailed.setStyle("-fx-font-weight: bold");

		setAlignment(Pos.CENTER);

		getChildren().addAll(btnBox, lblIntro1, btnIconAdmin, lblIntro, txtGp, lblConnectionFailed);


		setSpacing(20);
		setPadding(new Insets(20));
		setPrefSize(1300, 800);
		setStyle("-fx-background-color:white");
		
		btnHelp.setOnAction(event -> {
			OpenPdf pdf = new OpenPdf();
			pdf.openPdf("C:/Users/formation/Desktop/Workspace_Projet1/Donnees/ConnexionAdmin.pdf");
		});

		btnAdmin.setOnAction(event -> {
			try {
				BufferedReader bf = new BufferedReader(new FileReader(loginAdminPath));
				
				if (txtUsername.getText().toUpperCase().equals(bf.readLine().toUpperCase())) {
					if (txtPassword.getText().equals(bf.readLine())) {
				InterfaceAdmin interfaceAdmin = new InterfaceAdmin();
				Scene scene = new Scene(interfaceAdmin);
				scene.getStylesheets().add(getClass().getResource("../Stylesheet.css").toExternalForm());
				Stage stage = (Stage) this.getScene().getWindow();
				stage.setScene(scene);
					} else {
						txtPassword.setText("");
						lblConnectionFailed.setText("Mot de passe incorrect");
					}
				} else {
					txtUsername.setText("");
					txtPassword.setText("");
					lblConnectionFailed.setText("Identifiants et/ou Mot de passe incorrects");
					
				}
			} catch (IOException e) {
				
				e.printStackTrace();
			}

		});
		
		btnBack.setOnAction( event -> {

			InterfaceUser interfaceUser = new InterfaceUser();
			Scene scene = new Scene(interfaceUser);
			scene.getStylesheets().add(getClass().getResource("../Stylesheet.css").toExternalForm());
			Stage stage = (Stage) this.getScene().getWindow();
			stage.setScene(scene);
			
		});

	}
}
