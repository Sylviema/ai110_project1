package fr.eql.ai110.javafx.projet1.entities;

public class Stagiaire {

	private String lastName;
	private String firstName;
	private String postCode;
	private String groupName;
	private String yearGroup;
	private Integer leftChild;
	private Integer rightChild;
	private Integer indexStagiaire;
	private Integer indexRoot;

	public Stagiaire(String lastName, String firstName, String postCode, String groupName, String yearGroup) {
		this.lastName = lastName;
		this.firstName = firstName;
		this.postCode = postCode;
		this.groupName = groupName;
		this.yearGroup = yearGroup;
	}
	
	public Stagiaire(String lastName, String firstName, String postCode, String groupName, String yearGroup, Integer leftChild , Integer rightChild, Integer indexStagiaire, Integer indexRoot) {
		super();
		this.lastName = lastName;
		this.firstName = firstName;
		this.postCode = postCode;
		this.groupName = groupName;
		this.yearGroup = yearGroup;
		this.leftChild = leftChild;
		this.rightChild = rightChild;
		this.indexStagiaire = indexStagiaire;
		this.indexRoot = indexRoot;
	}
	

	public Stagiaire() {

	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	

	public int getLeftChild() {
		return leftChild;
	}

	public void setLeftChild(int leftChild) {
		this.leftChild = leftChild;
	}

	public int getRightChild() {
		return rightChild;
	}

	public void setRightChild(int rightChild) {
		this.rightChild = rightChild;
	}


	public String getYearGroup() {
		return yearGroup;
	}

	public void setYearGroup(String yearGroup) {
		this.yearGroup = yearGroup;
	}
	

	public int getIndexRoot() {
		return indexRoot;
	}

	public void setIndexRoot(int indexRoot) {
		this.indexRoot = indexRoot;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public int getIndexStagiaire() {
		return indexStagiaire;
	}

	public void setIndexStagiaire(int indexStagiaire) {
		this.indexStagiaire = indexStagiaire;
	}

//	@Override
//	public String toString() {
//		return "Stagiaire [lastName=" + lastName.toUpperCase() + ", firstName=" + firstName.toUpperCase() + ", postCode=" + postCode.toUpperCase()
//				+ ", groupName=" + groupName.toUpperCase() + ", yearGroup=" + yearGroup.toUpperCase() + ", leftChild=" + leftChild + ", rightChild="
//				+ rightChild + ", indexStagiaire=" + indexStagiaire + ", indexRoot=" + indexRoot + "]";
//	}

	@Override
	public String toString() {
		return "Nom : "+ lastName.toUpperCase() + "\r\nPrénom : " + firstName.toUpperCase() + "\r\nDépartement : " + postCode.toUpperCase()
				+ "\r\nNom de Promotion : " + groupName.toUpperCase() + "\r\nAnnée de Promotion : " + yearGroup.toUpperCase();
	}

	
	



	
	
	
	

}
