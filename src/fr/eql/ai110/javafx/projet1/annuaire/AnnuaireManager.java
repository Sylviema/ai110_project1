package fr.eql.ai110.javafx.projet1.annuaire;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import fr.eql.ai110.javafx.projet1.entities.Stagiaire;
import fr.eql.ai110.javafx.projet1.model.ToUpperCamelCase;

public class AnnuaireManager {

	private String SourceFilePath = "C:/Users/formation/Desktop/Workspace_Projet1/Donnees/stagiaires.txt";
	private String DestinationFilePath = "C:/Users/formation/Desktop/Workspace_Projet1/Donnees/stagiairesRAF.txt";
	private ToUpperCamelCase upperCamelCase = new ToUpperCamelCase();

	private static int lastNameFieldSize = 50;
	private static int firstNameFieldSize = 50;
	private static int postCodeFieldSize = 2;
	private static int groupNameFieldSize = 15;
	private static int yearGroupFieldSize = 4;
	private static int leftChildFieldSize = 10;
	private static int rightChildFieldSize = 10;
	private static int indexStagiaireFieldSize = 10;
	private static int indexRootFieldSize = 10;
	private Stagiaire rootStudent;
	private List<Stagiaire> sortedStagiaires = new ArrayList<Stagiaire>();

	private int numberOfStudent = 0;

	private int studentFieldSize = lastNameFieldSize + firstNameFieldSize + postCodeFieldSize + groupNameFieldSize
			+ yearGroupFieldSize + leftChildFieldSize + rightChildFieldSize + indexStagiaireFieldSize
			+ indexRootFieldSize;

	protected List<Stagiaire> readSourceFile() {

		List<Stagiaire> stagiaires = new ArrayList<Stagiaire>();

		
		try {
			BufferedReader source = new BufferedReader(new FileReader(SourceFilePath));
			while (source.ready()) {
				String lastName = source.readLine().trim();
				String firstName = source.readLine().trim();
				String postCode = source.readLine().trim();
				String groupName = source.readLine().trim();
				String yearGroup = source.readLine().trim();
				source.readLine();

				Stagiaire stagiaire = new Stagiaire(lastName, firstName, postCode, groupName, yearGroup, 0, 0, 0, 1);

				stagiaires.add(stagiaire);
				numberOfStudent++;

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return stagiaires;
	}

	public void formatRafData() {
		File file = new File(DestinationFilePath);
		if (!file.exists()) {
		List<Stagiaire> stagiaires = readSourceFile();
		writeDestinationFile(stagiaires);
		}
	}

	private void writeDestinationFile(List<Stagiaire> stagiaires) {
		RandomAccessFile raf;
		int index = 1;
		try {
			raf = new RandomAccessFile(DestinationFilePath, "rw");
			raf.seek(studentFieldSize);
			for (Stagiaire s : stagiaires) {
				writeOneStagiaire(s, raf, index);
				index++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void writeOneStagiaire(Stagiaire stagiaire, RandomAccessFile raf, int index) {
		writeStagiaireField(stagiaire.getLastName().toUpperCase(), lastNameFieldSize, raf);
		writeStagiaireField(upperCamelCase.toCamelCase(stagiaire.getFirstName()), firstNameFieldSize, raf);
		writeStagiaireField(stagiaire.getPostCode(), postCodeFieldSize, raf);
		writeStagiaireField(stagiaire.getGroupName().toUpperCase(), groupNameFieldSize, raf);
		writeStagiaireField(stagiaire.getYearGroup(), yearGroupFieldSize, raf);
		writeStagiaireField(String.valueOf(stagiaire.getLeftChild()), leftChildFieldSize, raf);
		writeStagiaireField(String.valueOf(stagiaire.getRightChild()), rightChildFieldSize, raf);
		writeStagiaireField(String.valueOf(index), indexStagiaireFieldSize, raf);
		writeStagiaireField(String.valueOf(stagiaire.getIndexRoot()), indexRootFieldSize, raf);
	}

	public void writeStagiaire(Stagiaire stagiaire, RandomAccessFile raf, int index) {
		try {
			raf.seek(index * studentFieldSize);
			writeOneStagiaire(stagiaire, raf, index);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void writeStagiaireField(String value, int size, RandomAccessFile raf) {
		try {
			raf.write(value.getBytes("ISO_8859_1"));
			int remainingSpaces = size - value.length();

			for (int i = 0; i < remainingSpaces; i++) {
				raf.write(" ".getBytes("ISO_8859_1"));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void buildBinaryTree() {
		try {
			RandomAccessFile raf = new RandomAccessFile(DestinationFilePath, "rw");

			Stagiaire rootStudent = readStudent(1, raf);
			Stagiaire currentStudent = new Stagiaire();

			for (int indexStudent = 2; indexStudent < numberOfStudent; indexStudent++) {
				currentStudent = readStudent(indexStudent, raf);
				compareStudent(currentStudent, rootStudent, raf, indexStudent);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void compareStudent(Stagiaire currentStagiaire, Stagiaire compareStagiaire, RandomAccessFile raf,
			int indexStudent) {

		if (currentStagiaire.getLastName().toUpperCase().compareTo(compareStagiaire.getLastName()) > 0) {

			if (hasARightChild(compareStagiaire)) {
				compareStudent(currentStagiaire, goToRightChild(compareStagiaire, raf), raf, indexStudent);

			} else {
				compareStagiaire.setRightChild(indexStudent);
				writeStagiaire(compareStagiaire, raf, compareStagiaire.getIndexStagiaire());
				currentStagiaire.setIndexRoot(compareStagiaire.getIndexStagiaire());
				writeStagiaire(currentStagiaire, raf, indexStudent);

			}
		} else {
			if (hasALeftChild(compareStagiaire)) {
				compareStudent(currentStagiaire, goToLeftChild(compareStagiaire, raf), raf, indexStudent);

			} else {

				compareStagiaire.setLeftChild(indexStudent);
				writeStagiaire(compareStagiaire, raf, compareStagiaire.getIndexStagiaire());
				currentStagiaire.setIndexRoot(compareStagiaire.getIndexStagiaire());
				writeStagiaire(currentStagiaire, raf, indexStudent);
			}
		}
	}

	public Stagiaire readStudent(int index, RandomAccessFile raf) {
		Stagiaire stagiaire = new Stagiaire();
		try {
			raf.seek(index * studentFieldSize);

			stagiaire.setLastName(readStudentField(lastNameFieldSize, raf).trim());
			stagiaire.setFirstName(readStudentField(firstNameFieldSize, raf).trim());
			stagiaire.setPostCode(readStudentField(postCodeFieldSize, raf).trim());
			stagiaire.setGroupName(readStudentField(groupNameFieldSize, raf).trim());
			stagiaire.setYearGroup(readStudentField(yearGroupFieldSize, raf).trim());
			stagiaire.setLeftChild(Integer.parseInt(readStudentField(leftChildFieldSize, raf).trim()));
			stagiaire.setRightChild(Integer.parseInt(readStudentField(rightChildFieldSize, raf).trim()));
			stagiaire.setIndexStagiaire(Integer.parseInt(readStudentField(indexStagiaireFieldSize, raf).trim()));
			stagiaire.setIndexRoot(Integer.parseInt(readStudentField(indexRootFieldSize, raf).trim()));

		} catch (IOException e) {
			e.printStackTrace();
		}
		return stagiaire;
	}

	private String readStudentField(int size, RandomAccessFile raf) {
		String result = "";
		byte[] b = new byte[size];
		try {
			raf.read(b, 0, size);
			result = new String(b, StandardCharsets.ISO_8859_1);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	
	public List<Stagiaire> sortByAlphabeticOrder() {
		try {
			RandomAccessFile raf = new RandomAccessFile(DestinationFilePath, "rw");

			rootStudent = readStudent(1, raf);
			sortTree(getRootStudent(rootStudent, raf, 2), raf);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return sortedStagiaires;

	}

	public Stagiaire getRootStudent(Stagiaire rootStudent, RandomAccessFile raf, int index) {
		if (rootStudent.getIndexRoot() != 1) {
			rootStudent = readStudent(index, raf);
			rootStudent = getRootStudent(rootStudent, raf, index + 1);
		}
		;
		return rootStudent;
	}

	private void sortTree(Stagiaire stagiaire, RandomAccessFile raf) {

		if (hasALeftChild(stagiaire)) {
			sortTree(goToLeftChild(stagiaire, raf), raf);
			checkTheRightChild(stagiaire, raf);
		} else {
			checkTheRightChild(stagiaire, raf);
		}

	}

	public void checkTheRightChild(Stagiaire stagiaire, RandomAccessFile raf) {
		if (hasARightChild(stagiaire)) {
			sortedStagiaires.add(stagiaire);
			sortTree(goToRightChild(stagiaire, raf), raf);
		}
		if (hasARightChild(stagiaire) == false) {
			sortedStagiaires.add(stagiaire);
		}
	}

	public Stagiaire getTheSmallestChild(Stagiaire stagiaire, RandomAccessFile raf) {
		if (hasALeftChild(stagiaire)) {
			stagiaire = getTheSmallestChild(goToLeftChild(stagiaire, raf), raf);
		}
		return stagiaire;
	}

	public Stagiaire getTheBiggestChild(Stagiaire stagiaire, RandomAccessFile raf) {
		if (hasARightChild(stagiaire)) {
			stagiaire = getTheBiggestChild(goToRightChild(stagiaire, raf), raf);
		}
		return stagiaire;
	}

	public Stagiaire goToLeftChild(Stagiaire stagiaire, RandomAccessFile raf) {
		return readStudent(stagiaire.getLeftChild(), raf);
	}

	public Stagiaire goToRightChild(Stagiaire stagiaire, RandomAccessFile raf) {
		return readStudent(stagiaire.getRightChild(), raf);
	}

	public boolean hasARightChild(Stagiaire stagiaire) {
		if (stagiaire.getRightChild() != 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean hasALeftChild(Stagiaire stagiaire) {
		if (stagiaire.getLeftChild() != 0) {
			return true;
		} else {
			return false;
		}
	}

	public int getStudentFieldSize() {
		return studentFieldSize;
	}

	public String getDestinationFilePath() {
		return DestinationFilePath;
	}

	
	

}
