package fr.eql.ai110.javafx.projet1.ihm;

import fr.eql.ai110.javafx.projet1.annuaire.AnnuaireManager;
import fr.eql.ai110.javafx.projet1.model.user.InterfaceUser;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Ihm extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		InterfaceUser boutonInterfaceUser = new InterfaceUser();

		Scene scene = new Scene(boutonInterfaceUser, 1300, 800);

		primaryStage.setScene(scene);
		scene.getStylesheets().add(getClass().getResource("../model/Stylesheet.css").toExternalForm());
		primaryStage.setTitle("Annuaire des stagiaires");
		primaryStage.sizeToScene();
		primaryStage.show();

	}

	public static void main(String[] args) {
		AnnuaireManager annuaire = new AnnuaireManager();
		annuaire.formatRafData();
		annuaire.buildBinaryTree();
		launch(args);

	}

}
